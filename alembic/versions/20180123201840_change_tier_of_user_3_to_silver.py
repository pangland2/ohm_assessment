"""Change tier of user 3 to silver

Revision ID: 49ada1a6d158
Revises: 151558b524bb
Create Date: 2018-01-23 20:18:40.668156

"""

# revision identifiers, used by Alembic.
revision = '49ada1a6d158'
down_revision = '151558b524bb'

from alembic import op
import sqlalchemy as sa


def upgrade():
    connection = op.get_bind();
    result = connection.execute("""
    UPDATE user
    SET tier = 'Silver'
    WHERE user_id = 3;
    """)


def downgrade():
    connection = op.get_bind();
    result = connection.execute("""
    UPDATE user
    SET tier = 'Carbon'
    WHERE user_id = 3;
    """)

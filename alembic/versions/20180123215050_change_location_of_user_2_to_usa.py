"""Change location of user 2 to USA

Revision ID: 297c4f4dec14
Revises: 49ada1a6d158
Create Date: 2018-01-23 21:50:50.014654

"""

# revision identifiers, used by Alembic.
revision = '297c4f4dec14'
down_revision = '49ada1a6d158'

from alembic import op
import sqlalchemy as sa


def upgrade():
    connection = op.get_bind();
    result = connection.execute("""
        INSERT INTO rel_user (user_id, rel_lookup, attribute)
        VALUES (2, 'LOCATION', 'USA')
    """)


def downgrade():
    connection = op.get_bind();
    result = connection.execute("""
        DELETE FROM rel_user
        WHERE user_id = 2
    """)

"""Increase user point value to 1000

Revision ID: 151558b524bb
Revises: 00000000
Create Date: 2018-01-23 19:29:02.592920

"""

# revision identifiers, used by Alembic.
revision = '151558b524bb'
down_revision = '00000000'

from alembic import op
import sqlalchemy as sa


def upgrade():
    connection = op.get_bind();
    result = connection.execute("""
    UPDATE user
    SET point_balance = 1000
    ORDER BY user_id
    LIMIT 1
    """)

def downgrade():
    connection = op.get_bind();
    result = connection.execute("""
    UPDATE user
    SET point_balance = 0
    ORDER BY user_id
    LIMIT 1
    """)
